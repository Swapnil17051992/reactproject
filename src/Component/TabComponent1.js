import React from 'react';
import ReactDOM from 'react-dom'
import '../index.css'
import { BrowserRouter, Link ,Route} from 'react-router-dom';

// export const London=()=>(
//     <div>
//         <Link to="/about" className={'tablinks'} >About</Link>
//     <button className={'tablinks'} >London</button>
//     </div>
// )

// export const Paris=()=>(
//     <button className={'tablinks'} >Paris</button>
// )

export const App=()=>(
    <div>
        <Link to="/about">About</Link>
   <h2>App</h2>
   </div>
)

export const About=()=>(
   <div>
   <Link to="/">App</Link>
   <h2>About</h2>
</div>
   
)

export class Tab1 extends React.Component{
    render(){
        return(
            <BrowserRouter>
            <div><Route path='/' Component={App} ></Route>
               <Route path='/about' Component={About} ></Route>
                </div> 
            </BrowserRouter>
        )
    }
}