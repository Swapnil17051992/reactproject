import React from 'react';
import ReactDOM from 'react-dom'

import axios from 'axios';
import $ from 'jquery';

import 'bootstrap/dist/css/bootstrap.min.css';

export class Save extends React.Component{
    constructor(){
        super();
        this.onchangename=this.onchangename.bind(this);
        this.onchangegender=this.onchangegender.bind(this);
        this.onchangephone=this.onchangephone.bind(this);
        this.onchangeemail=this.onchangeemail.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.state={
            name:'',
            phone:'',
            gender:'',
            email:''
        }
    }

    onchangeemail(e){
        this.setState({
        email:e.target.value
        })
    }

    onchangegender(e){
        this.setState({
            gender:e.target.value
        })
    }

    onchangename(e){
        this.setState({
            name:e.target.value
        })
    }

    onchangephone(e){
        this.setState({
            phone:e.target.value
        })
    }

    onSubmit(e){
        debugger;
        e.preventDefault();
       const contacts={
           name:this.state.name,
           phone:this.state.phone,
           gender:this.state.gender,
           email:this.state.email
       };

       
       axios.post(`http://localhost:8080/api/contacts`, contacts)
      .then(res => {
          debugger;
        console.log(res);
        console.log(res.data);
      })
    }

    render(){
        return(
            <div style={{marginTop: 10}} className="row">
                <div className="col-lg-6">
                    <h3>Add New Business</h3>
                    <form  onSubmit={this.onSubmit}>
            
                <div className="form-group">
                    <label>Add Person Name:  </label>
                    <input type="text"
                     className="form-control"
                     value={this.state.name}
                     onChange={this.onchangename}
                     />
                </div>
                <div className="form-group">
                    <label>Add Email Name: </label>
                    <input type="text" 
                    className="form-control"
                    value={this.state.email}
                    onChange={this.onchangeemail}
                    />
                </div>
                <div className="form-group">
                    <label>Add Gender : </label>
                    <input type="text" 
                    className="form-control"
                    value={this.state.gender}
                    onChange={this.onchangegender}
                    />
                </div>
                <div className="form-group">
                    <label>Add Phone : </label>
                    <input type="text" 
                    className="form-control"
                    value={this.state.phone}
                    onChange={this.onchangephone}
                    />
                </div>
                <div className="form-group">
                    <input type="submit" value="Register Business" className="btn btn-primary"/>
                </div>
            </form>
                </div>
                <div className="col-lg-6">
                <h1>grid</h1>
                </div>
            </div>
   
        )
    }
}