    import React from "react";
    import ReactDOM from 'react-dom';

    import axios from 'axios';
    import $ from 'jquery';

    import 'bootstrap/dist/css/bootstrap.min.css';
    import {TableRow} from './table.component'
export class Users extends React.Component{
    constructor(){
        super();
        this.onChangeusername=this.onChangeusername.bind(this);
        this.onChangefirstname=this.onChangefirstname.bind(this);
        this.onChangelastname=this.onChangelastname.bind(this);
        this.onChangeemail=this.onChangeemail.bind(this);
        this.onChangephone=this.onChangephone.bind(this);
        this.onChangegender=this.onChangegender.bind(this);
        this.onChangepassword=this.onChangepassword.bind(this);
        this.onSubmit=this.onSubmit.bind(this);
        this.onUpdate=this.onUpdate.bind(this);
        this.onDelete=this.onDelete.bind(this);
        this.triggerChildAlert=this.triggerChildAlert.bind(this);
        this.state={
            username:'',
            firstname:'',
            lastname:'',
            email:'',
            password:'',
            gender:'',
            phone:'',
            id:''
            }
     

    }

 
    myCallback = (dataFromChild) => {
        debugger;
        console.log(dataFromChild);
        this.setState({
            username:dataFromChild.username,
            firstname:dataFromChild.firstname,
            lastname:dataFromChild.lastname,
            email:dataFromChild.email,
            phone:dataFromChild.phone,
            gender:dataFromChild.gender,
            password:dataFromChild.password,
            id:dataFromChild._id
            
        })
        
        //[...we will use the dataFromChild here...]
    }
    
    onChangeusername(e){
        debugger;
        this.setState({
            username:e.target.value
        });
    }
    onChangefirstname(e){
        debugger;
        this.setState({
            firstname:e.target.value
        })
    }

    onChangelastname(e){
        debugger;
        this.setState({
            lastname:e.target.value
        })
    }

    onChangeemail(e){
        debugger;
        this.setState({
            email:e.target.value
        })
    }

    onChangephone(e){
        this.setState({
            phone:e.target.value
        })
    }

    onChangegender(e){
        debugger;
        this.setState({
            gender:e.target.value
        })
    }

    onChangepassword(e){
        debugger;
        this.setState({
            password:e.target.value
        })
    }

    onSubmit(e){
        e.preventDefault();
        const users={
            username:this.state.username,
            firstname:this.state.firstname,
            lastname:this.state.lastname,
            email:this.state.email,
            phone:this.state.phone,
            gender:this.state.gender,
            password:this.state.password

        };

        axios.post("http://localhost:8080/api/users",users).then(res=>{
            debugger;
            console.log(res.data);
            this.triggerChildAlert();
        }).catch(console.error()
        );
    }

    onUpdate(e){
        debugger;
        e.preventDefault();
        const users={
            username:this.state.username,
            firstname:this.state.firstname,
            lastname:this.state.lastname,
            email:this.state.email,
            phone:this.state.phone,
            gender:this.state.gender,
            password:this.state.password,
            id:this.state.id

        };

        axios.put("http://localhost:8080/api/users/"+users.id+"",users).then(res=>{
            debugger;
            console.log(res.data);
            this.triggerChildAlert();
        }).catch(console.error()
        );
    }
    triggerChildAlert(){
        this.refs.child.showAlert();
    }

onDelete(e){
    e.preventDefault();

    axios.delete("http://localhost:8080/api/users/"+this.state.id).then(res=>{
        debugger;
            console.log(res.data);
            this.triggerChildAlert();

    }).catch(console.error())
   
}

    render(){
        const {username}=this.state
        const {firstname}=this.state
        const {lastname}=this.state
        const {email}=this.state
        const {phone}=this.state
        const {password}=this.state
        const {gender}=this.state
        return(
            <div style={{marginTop: 10}} className="row">
            <div className="col-lg-6">
            <h2>User Details</h2>
                
                    <div className="form-group col-lg-12">
                        <label>User Name</label>
                        <input type="text" className="form-control" value={username} onChange={this.onChangeusername}/>
                    </div>
                    <div className="form-group col-lg-12">
                        <label>First Name</label>
                        <input type="text" className="form-control" value={firstname} onChange={this.onChangefirstname}/>
                    </div>
                    <div className="form-group col-lg-12">
                        <label>Last Name</label>
                        <input type="text" className="form-control" value={lastname} onChange={this.onChangelastname}/>
                    </div>
                    <div className="form-group col-lg-12">
                        <label>Email</label>
                        <input type="email" className="form-control" value={email} onChange={this.onChangeemail}/>
                    </div>
                    <div className="form-group col-lg-12">
                        <label>Phone No</label>
                        <input type="text" className="form-control" value={phone} onChange={this.onChangephone}/>
                    </div>
                    <div className="form-group col-lg-12">
                        <label>Gender</label>
                       <select className="form-control"  onChange={this.onChangegender}>
                             <option value={gender}>---Select--- </option>
                            <option value={gender}>Male </option>
                            <option value={gender}>Female </option>
                       </select>
                        
                    </div>
                    <div className="form-group col-lg-12">
                        <label>Password</label>
                        <input type="pasword" className="form-control" value={password} onChange={this.onChangepassword}/>
                    </div>
                    <div className="form-group col-lg-12">
                    <input type="submit" value="User Register" onClick={this.onSubmit} className="btn btn-primary"/>
                    &nbsp;
                    <input type="submit" value="User Update" onClick={this.onUpdate} className="btn btn-primary"/>

                    &nbsp;
                    <input type="submit" value="Delete" onClick={this.onDelete} className="btn btn-primary"/>
                </div>
                   
                
            </div>
            
            <div className="col-lg-6">
            <h1>grid</h1>
            <TableRow ref="child" callbackFromParent={this.myCallback} />
            </div>
            </div>
        )
    }
}
 