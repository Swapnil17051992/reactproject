import React ,{Component} from 'react';

import axios from 'axios';
import $ from 'jquery';

import 'bootstrap/dist/css/bootstrap.min.css';
export class Index extends React.Component{

    constructor(){
        super();
        this.state={
            users:[]
        }
        
    }

    componentDidMount(){
        // axios.get("http://localhost:8080/api/contacts/").then(res=>{
        //     debugger;
        //     const user=res.data;
        //     this.setState({
        //         user:user
        //     });
        // });
        $.ajax({
            type:'GET',
            contentType: 'application/json; charset=utf-8',
            url:'http://localhost:8080/api/contacts',//'https://jsonplaceholder.typicode.com/users',
            success:(data)=>{
                this.setState({
                    users:data.data
                })
            }
        })
    }
    render(){
        return(
            <div>
                <p>Welcome to Index Component</p>
                <table className='table table-striped'>
                    <thead>
                    <div className='row'>
                           <div> <th>Person name</th></div>
                           <div>  <th>Person Email</th></div>
                           <div>  <th>Gender</th></div>
                           <div>  <th>Person Phone</th></div>
                           </div>
                    </thead>
                    <tbody>
                    {
                        this.state.users.map((val,ind)=>{
                            return <ChildIndex Index={ind} details={val}/>
                        })
                    }
                    </tbody>
                </table>
            </div>
        )
    }
}

class ChildIndex extends  React.Component{
    render(){
        return(
            <div>
            <td>{this.props.details.name}</td>
            <td>{this.props.details.email}</td>
            <td>{this.props.details.gender}</td>
            <td>{this.props.details.phone}</td>
            </div>
        )
    }
}
