import React ,{component} from 'react';

export class Create extends React.Component{

        constructor(){
            super();
            this.onChangePerson_Name=this.onChangePerson_Name.bind(this);
            this.onChangebusiness_name=this.onChangebusiness_name.bind(this);
            this.onChangeBusiness_gst_number=this.onChangeBusiness_gst_number.bind(this);
            this.onSubmit=this.onSubmit.bind(this);
            this.state={
                person_name:'',
                business_name:'',
                business_gst_number:''
            }


        }

        onChangePerson_Name(e){
            debugger;
            this.setState({
                person_name:e.target.value
            })
        }

        onChangebusiness_name(e){
            this.setState({
                business_name:e.target.value
            })
        }

        onChangeBusiness_gst_number(e){
            this.setState({
                business_gst_number:e.target.value
            })
        }

        onSubmit(e){
            e.preventDefault();
           // console.log(`The values are ${this.state.person_name}, ${this.state.business_name}, and ${this.state.business_gst_number}`)
            
            this.setState({
                    person_name:'',
                    business_name:'',
                    business_gst_number:''
            })
        }
    render(){
        return (
            <div style={{marginTop: 10}}>
                <h3>Add New Business</h3>
                <form  onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Add Person Name:  </label>
                        <input type="text"
                         className="form-control"
                         value={this.state.person_name}
                         onChange={this.onChangePerson_Name}
                         />
                    </div>
                    <div className="form-group">
                        <label>Add Business Name: </label>
                        <input type="text" 
                        className="form-control"
                        value={this.state.business_name}
                        onChange={this.onChangebusiness_name}
                        />
                    </div>
                    <div className="form-group">
                        <label>Add GST Number: </label>
                        <input type="text" 
                        className="form-control"
                        value={this.state.business_gst_number}
                        onChange={this.onChangeBusiness_gst_number}
                        />
                    </div>
                    <div className="form-group">
                        <input type="submit" value="Register Business" className="btn btn-primary"/>
                    </div>
                </form>
            </div>
        )
    }
}