import React from "react";
import ReactDOM from 'react-dom';

import axios from 'axios';
import $ from 'jquery';
import '../index.css'
import 'bootstrap/dist/css/bootstrap.min.css';

export class TableRow extends React.Component{
    constructor(){
        super();
        this.bindData=this.bindData.bind(this);
        this.state={
            data:[]
        }
    }
    someFn = (val) => {
        debugger;
      //  [...somewhere in here I define a variable listInfo which    I think will be useful as data in my ToDoList component...]
        this.props.callbackFromParent(val);
    }

    componentDidMount(){
        debugger;
        this.bindData();
        
    }
    showAlert() {
        alert('Updated Successfully!!!');
        this.bindData();
    }

    bindData(){
        $.ajax({
            type:'GET',
            contentType: 'application/json; charset=utf-8',
            url:'http://localhost:8080/api/users',//'https://jsonplaceholder.typicode.com/users',
            success:(data)=>{
                this.setState({
                    data:data.data
                })
            }
        })
    }

    render(){
        
        let rows=this.state.data.map((user,index)=>{
            return <TableTDRow index={index} key={index}
            details={user} clickhandler={this.someFn}
            />
        })
        return(
            <table>
            <thead>
            <tr>
            <th>Action</th>
            <th>User Name</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>Phone</th>
            </tr>
            </thead>
                <tbody>
                    {
                        rows
                    }
                </tbody>
            </table>
        )
    }
}

const TableTDRow=(props)=>{
    return(
        <tr>
        <td><button type="submit" onClick={()=>{
            props.clickhandler(props.details)
        }
        } >Edit</button></td>
            <td>
                {props.details.username}
            </td>
            <td>
                {props.details.firstname}
            </td>
            <td>
                {props.details.lastname}
            </td>
            <td>
                {props.details.email}
            </td>
            <td>
                {props.details.gender}
            </td>
            <td>
                {props.details.phone}
            </td>
        </tr>
    );
}