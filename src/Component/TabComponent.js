import React from 'react';
import ReactDOM from 'react-dom'
import '../index.css'
export class TabComponent extends React.Component{

    constructor(){
        super();
        this.state={}
        this.tabLondon=this.tabLondon.bind(this);
        this.tabParis=this.tabParis.bind(this);

        
    }
    tabLondon(evt){
        debugger;
        evt.preventDefault();
        this.tab1();
            
    }

    tab1(){
        return(
            <div id="Paris" className={'tabcontent'}>
            <span  className={'topright'}>&times</span>
            <h3>Paris</h3>
            <p>Paris is the capital of France.</p> 
        </div>
        )
    }

    tabParis(evt){
        evt.preventDefault();
            return(
                <div id="London" className={'tabcontent'}>
                <span className={'topright'}>&times</span>
                <h3>London</h3>
                <p>London is the capital city of England.</p>
            </div>
            )
    }

    render(){
    return(
        <div>                    
               <h2>Tabs</h2>
                <p>Click on the x button in the top right corner to close the current tab:</p>

             <div className={'tab'}>
                <button className={'tablinks'} id="defaultOpen" onClick={this.tabLondon}>London</button>
                <button className={'tablinks'} onClick={this.tabParis}>Paris</button>
                <button className={'tablinks'} >Tokyo</button>
            </div>

            

           

            <div id="Tokyo" className={'tabcontent'}>
                <span className={'topright'}>&times</span>
                <h3>Tokyo</h3>
                <p>Tokyo is the capital of Japan.</p>
            </div>
        </div>
        )
    }
}
