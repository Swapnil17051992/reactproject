import React from 'react'
import ReactDOM from 'react-dom'

import '../style/login.css'
import Axios from 'axios';
import {Users} from './user';
import { Redirect,Link ,history} from 'react-router-dom'
import { App } from '../app';
export class  Login extends React.Component{
    constructor(){
        super();
        this.onSubmit=this.onSubmit.bind(this);
        this.onChange_email=this.onChange_email.bind(this);
        this.onChange_password=this.onChange_password.bind(this);
        this.fnClearData=this.fnClearData.bind(this);
        //this.textInput=React.createRef();
        this.clearElement=this.clearElement.bind(this);
        this.fnLogincheck=this.fnLogincheck.bind(this);
       // this.bindusers=this.bindusers.bind(this);
        this.fntoggleform=this.fntoggleform.bind(this);
        this.fnbindSignupForm=this.fnbindSignupForm.bind(this)
        this.fnbindloginfrom=this.fnbindloginfrom.bind(this)
        this.state={
            firstname:'',
            lastname:'',
            dob:'',
            email:'',
            password:'',
            IsEditing:false,
            IsSigned:false
        }
    }

    fntoggleform(){
        debugger;
        const {IsEditing}=this.state
        this.setState({
            IsEditing:!IsEditing
        })
    }
    clearElement(){
       // e.preventDefault();
        this.refs.fieldfirstName.value='';
        this.refs.fieldlastName.value='';
        this.refs.fielddob.value='';
        this.refs.fieldemail.value='';
        this.refs.fieldpasswordName.value=''
    }
    onSubmit(e){
        debugger;
        e.preventDefault();
       
        const signup={
            firstname:this.state.firstname,
            lastname:this.state.lastname,
            dob:this.state.dob,
            email:this.state.email,
            password:this.state.password

        };

        Axios.post("http://localhost:8080/api/signups",signup).then(res=>{
            console.log(res.data);
            this.AlertMessage("Signup successfully !!");
            this.clearElement();
        }).catch(console.error())
    }
    

    fnLogincheck(e){
        debugger;
        e.preventDefault();

        const signup={
            email:this.state.email,
            password:this.state.password
        }

        Axios.get("http://localhost:8080/api/signup/"+this.state.email+"/"+this.state.password).then(res=>{
            if(res.data.status!='success')
            {
                    this.AlertMessage("Login Failed !!");
            }
            else
            {
                this.setState(() => ({
                    IsSigned: true
                  }))
               // return(<Users/>)  
              // this.renderRedirect();  
              // this.history.pushState(null, 'save');         
            }
        }).catch(console.error())
    }


 
    onChange_email(e){
        this.setState({
            email:e.target.value
        })
    }

    onChange_password(e){
        this.setState({
            password:e.target.value
        })
    }

    AlertMessage(value){
    alert(value);
    }

    fnClearData(){
        this.setState({
            firstname:''
        })
    }

    fnbindSignupForm(){
        return(
            <div className="col-lg-6 col-md-6 login-form-1" style={{float:"left"}}>
            <div className="row">
            <h3>Sign-Up</h3>
            <div className="col-lg-12">
            <form>
                <div className="form-group">
                    <input type="text" className="form-control" ref="fieldfirstName" onChange={e=>this.setState({firstname:e.target.value})} placeholder="First Name *"  />
                </div>
            <div className="form-group">
                <input type="text" className="form-control" ref="fieldlastName"
                onChange={e=>this.setState({lastname:e.target.value})} placeholder="Last Name *" />
             </div>
             <div className="form-group">
             <input type="date" className="form-control" ref="fielddob"
              onChange={e=>this.setState({dob:e.target.value})} placeholder="Date of Birth *" />
          </div>
                <div className="form-group">
                    <input type="email" className="form-control" ref="fieldemail" onChange={this.onChange_email} placeholder="Your Email *"  />
                </div>
                <div className="form-group">
                    <input type="password" className="form-control" ref="fieldpasswordName" onChange={this.onChange_password} placeholder="Your Password *" />
                </div>
                <div className="form-group">
                    <input type="submit" className="btnSubmit" onClick={this.onSubmit} value="Sign Up" style={{float:"right"}}/>
                </div>
               
                <div className="form-group">
                <a href="#" className="ForgetPwd" onClick={this.fntoggleform} >Go to Login</a>
                </div>
            </form>
            </div>
            </div>
            </div>
        )
    }

    fnbindloginfrom(){
        return(
            <div className="col-lg-6 col-md-6 login-form-2" style={{float:"right"}}>
            <div className="row ">
            <h3>Login</h3>
            <div className="col-lg-12">
            <form>
                <div className="form-group">
                    <input type="email" className="form-control" 
                    onChange={e=>this.setState({email:e.target.value})} placeholder="Your Email *"  />
                </div>
                <div className="form-group">
                    <input type="password"  className="form-control"
                    onChange={e=>this.setState({password:e.target.value})}
                    placeholder="Your Password *"  />
                </div>
                <div className="form-group">
                    <input type="submit" className="btnSubmit" value="Login" onClick={this.fnLogincheck} />
                 &nbsp;&nbsp;   <a href="#" className="ForgetPwd" >Forget Password?</a>
                </div>
                <div className="form-group">
                
                <a href="#" className="ForgetPwd" onClick={this.fntoggleform}  >Please Sign-Up Here?</a>
                   
                </div>
            </form>
            </div>
            </div>
        </div>
        )
    }
    render(){
        const {IsEditing}=this.state;
        const {IsSigned}=this.state;
        if (this.state.IsSigned) {
            return <App/>
          }
          else{
        
        return(
            <div className="container login-container">
            
           {
            IsEditing?this.fnbindSignupForm():this.fnbindloginfrom()
           }
            </div>

        
       
        )
          }
    }
}
