
import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import {Create} from './Component/create.component';
import {Edit} from './Component/edit.component';
import {Index} from './Component/index.component';
import {Save} from './Component/save';


import { Navbar, Nav, NavItem, NavDropdown, MenuItem } from 'react-bootstrap';
import {$} from 'jquery'
import { Users } from './Component/user';

import {Login} from './Component/login'
export class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
        <div>
        <Navbar  bg="light" expand="lg">
  <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav">
    <Nav className="mr-auto">
      <Nav>
      <Link to={'/'} className="nav-link">Home</Link>
      </Nav> 

      <Nav>
      <Link to={'/save'} className="nav-link">Link</Link>
      </Nav> 
 
      <NavDropdown title="Dropdown" id="basic-nav-dropdown">
        <NavDropdown.Item>
        <b><Link to={'/user'} className="nav-link">User Info</Link></b>
        </NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown>
    </Nav>
    
  </Navbar.Collapse>
</Navbar></div><br/>
          
          <Switch>
          
              <Route  path='/login' component={ Login } />
              <Route exact  path='/save' component={ Save } />
              <Route path='/edit/:id' component={ Edit } />
              <Route  path='/index' component={ Index } />
              <Route path='/user' component={ Users } />
          </Switch>
        </div>
      </Router>
    );
  }
}


